﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSanatorium.Api
{
    [Table("Кли_Пут")]
    public class ClientPermit :Entity, IEntity
    {
        [Column("Код_кли_пут")]
        public int Id { get; set; }

        public virtual Client Client { get; set; } 

        public virtual Permit Permit { get; set; }

        public  virtual Room Room { get; set; }

        [Column("Дата_заезда")]
        [Display(Name = "Дата заезда")]
        public DateTime BeginDate { get; set; }

        [Column("Дата_выезда")]
        [Display(Name = "Дата выезда")]
        public DateTime EndDate { get; set; }
    }

    public class ClientPermitMap : EntityTypeConfiguration<ClientPermit>
    {
        public ClientPermitMap()
        {
            HasRequired(x => x.Client).WithMany(x=>x.ClientPermits).Map(x => x.MapKey("Код_клиента"));

            HasRequired(x => x.Permit).WithOptional().Map(x => x.MapKey("Код_путевки"));

            HasRequired(x => x.Room).WithMany().Map(x => x.MapKey("Код_номера"));
        }
    }
}
