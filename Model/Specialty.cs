﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSanatorium.Api
{
    /// <summary>
    /// Медицинская специальность
    /// </summary>
    [Table("Специальность")]
    public class Specialty : Entity, IEntity
    {
        [Column("Код_специальности")]
        public int Id { get; set; }

        [Column("Название")]
        [Display(Name = "Наименование специальности")]
        public string Name { get; set; }

        /// <summary>
        /// Наименование услуги
        /// </summary>
        [Column("Описание")]
        [Display(Name = "Услуга")]
        public string Service { get; set; }

        [Display(Name = "Специалисты")]
        public virtual List<Doctor> Doctors { get; set; }
    }
    public class SpecialtyMap : EntityTypeConfiguration<Specialty>
    {
        public SpecialtyMap()
        {
        }
    }
}
