﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebSanatorium.Api
{
    /// <summary>
    /// Заявка на путевку в санатории
    /// </summary>
    [Table("Заявка")]
    public class Order : Entity, IEntity
    {
        [Column("Код_заявки")]
        public int Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        [Column("ФИО")]
        [Display(Name = "ФИО")]
        public string FullName { get; set; }

        /// <summary>
        /// Телефон заказчика
        /// </summary>
        [Column("Телефон")]
        [Display(Name = "Телефон")]
        public string Phone { get; set;}

        /// <summary>
        /// Эл. адрес
        /// </summary>
        [Column("эл_почта")]
        [Display(Name = "Эл. адрес")]
        public string Email { get; set; }

        /// <summary>
        /// Предполагаемая дата заезда
        /// </summary>
        [Column("ДатаЗаезда")]
        [Display(Name = "Желаемая дата заезда")]
        public DateTime InputDate { get; set; }

        /// <summary>
        /// Количество взрослых
        /// </summary>
        [Column("КоличествоВзрослых")]
        [Display(Name = "К-во взрослых")]
        public uint AdultCount { get; set; }

        /// <summary>
        /// Количество детей
        /// </summary>
        [Column("КоличествоДетей")]
        [Display(Name = "К-во детей")]
        public uint ChildrenCount { get; set; }

        /// <summary>
        /// Вид путевки (с лечением или без)
        /// </summary>
        [Column("ВключаяЛечение")]
        [Display(Name = "С лечением")]
        public bool IsWithTherapy { get; set; }

        /// <summary>
        /// Если заказ одобрен, то с ним должен быть связан клиент
        /// </summary>
        public virtual Client Client { get; set; }

        /// <summary>
        /// Заказ одобрен, т.к. с ним связан клиент
        /// </summary>
        [Column("ЗаявкаПодтверждена")]
        [Display(Name = "Заявка подтверждена")]
        public bool IsApproved { get; set; }
    }

    public class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap()
        {
            HasOptional(x => x.Client).WithOptionalPrincipal(x=>x.Order);
        }
    }
}
