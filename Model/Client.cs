﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebSanatorium.Api
{
    /// <summary>
    /// Клиент санатория
    /// </summary>
    [Table("Клиент")]
    public class Client : Entity, IEntity
    {
        [Column("Код_клиента")]
        public int Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        [Column("ФИО_клиента")]
        [Display(Name = "ФИО")]
        public string FullName { get; set; }

        /// <summary>
        /// Телефон заказчика
        /// </summary>
        [Column("Телефон")]
        [Display(Name = "Телефон")]
        public string Phone { get; set;}

        /// <summary>
        /// Заявка на размещение
        /// </summary>
        public virtual Order Order { get; set; }

        /// <summary>
        /// Путевки купленные клиентом
        /// </summary>
        public virtual ICollection<ClientPermit> ClientPermits { get; set; }
    }

    public class ClientMap : EntityTypeConfiguration<Client>
    {
        public ClientMap()
        {
            HasOptional(x => x.Order).WithOptionalDependent(x=>x.Client).Map(x => x.MapKey("order_id"));

        }
    }
}
