﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSanatorium.Api
{
    /// <summary>
    /// Класс номера
    /// </summary>
    [Table("Тип_номера")]
    public class RoomClass : Entity, IEntity
    {
        [Column("Код_тип_номера")]
        public int Id { get; set; }

        [Column("Название")]
        [Display(Name = "Класс номера")]
        public string Name { get; set; }
    }
}
