﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebSanatorium.Api
{
    /// <summary>
    /// Номер проживания в гостинице
    /// </summary>
    [Table("Номер")]
    public class Room : Entity, IEntity
    {
        [Column("Код_номера")]
        public int Id { get; set; }

        /// <summary>
        /// Номер комнаты
        /// </summary>
        [Column("Номер_комнаты")]
        [Display(Name ="Номер комнаты")]
        public string Number { get; set; }

        /// <summary>
        /// Файл с фотографией номера
        /// </summary>
        [Column("Имя_файла")]
        [Display(Name = "Изображение")]
        public string PictureFile { get; set; }

        /// <summary>
        /// Класс номера
        /// </summary>
        [Display(Name = "Класс номера")]
        public virtual RoomClass RoomClass { get; set; }
    }

    public class RoomMap : EntityTypeConfiguration<Room>
    {
        public RoomMap()
        {
            HasRequired(x => x.RoomClass).WithMany().Map(x => x.MapKey("Код_тип_номера"));
        }
    }
}
