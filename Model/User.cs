﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebSanatorium.Api
{
    /// <summary>
    /// Заявка на путевку в санатории
    /// </summary>
    [Table("Пользователь")]
    public class User : Entity, IEntity
    {
        [Column("Код_пользователя")]
        [Index]
        public int Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        [Column("ФИО")]
        public string FullName { get; set; }

        /// <summary>
        /// login
        /// </summary>
        [Column("Логин")]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Column("Пароль")]
        public string Password { get; set;}

    }
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
        }
    }
}
