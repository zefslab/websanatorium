﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebSanatorium.Api
{
    /// <summary>
    /// Врач
    /// </summary>
    [Table("Врач")]
    public class Doctor : Entity, IEntity
    {
        [Column("Код_врача")]
        public int Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        [Column("ФИО_врача")]
        [Display(Name="ФИО сотрудника")]
        public string FullName { get; set; }
        
        /// <summary>
        /// Возраст
        /// </summary>
        [Column("Возраст")]
        [Display(Name = "Возраст")]
        public DateTime Old { get; set; }

        /// <summary>
        /// Фото
        /// </summary>
        [Column("Фотография")]
        [Display(Name = "Фотография")]
        public string Photo { get; set; }

        /// <summary>
        /// Медицинская специальность
        /// </summary>
        [Display(Name="Специальности")]
        public virtual List<Specialty> Specialties { get; set;}
        
    }

    public class DoctorMap : EntityTypeConfiguration<Doctor>
    {
        public DoctorMap()
        {
            HasMany(a => a.Specialties)
             .WithMany(x=>x.Doctors)
             .Map(x =>
             {
                 x.MapLeftKey("Код_врач");
                 x.MapRightKey("Код_специальсности");
                 x.ToTable("Врач_Спец");
             });
        }
    }
}
