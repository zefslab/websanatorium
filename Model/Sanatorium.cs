﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace WebSanatorium.Api
{
    /// <summary>
    /// Информация о санатории хранится в xml файле
    /// </summary>
    public class Sanatorium
    {
        private static Sanatorium _sanatorium;

        /// <summary>
        /// Файл в котором хранится информация о санатории 
        /// </summary>
        private static string DataFileName { get; set; }

        #region Buisness properties

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Телефоны контактные
        /// </summary>
        public List<string> Phones { get; set; }

        /// <summary>
        /// О санатории
        /// </summary>
        public string About { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на представительную фотографию
        /// </summary>
        public string MainPicture { get; set; }

        /// <summary>
        /// Названия файлов фотогалереи 
        /// </summary>
        public List<string> Pictures { get; set; }

        /// <summary>
        /// Предоставляемый досуг
        /// </summary>
        public List<string> RestServices { get; set; }
        #endregion

        public static Sanatorium GetInstance()
        {
            if (_sanatorium == null)
            {
                DataFileName = ConfigurationManager.AppSettings["DataFile"];

                if (File.Exists(DataFileName))
                {
                    //Метод read может вернуть null, если файл с данными окажется пустым
                    _sanatorium = Read() ?? new Sanatorium();
                }
                else
                {
                    //Файл с конфигурационной информацией отсутсвует, поэтому нужно создать его;
                    _sanatorium = new Sanatorium();

                    Save(_sanatorium);
                }
            }

            return _sanatorium;
        }
        private static Sanatorium Read()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Sanatorium));

            Sanatorium sanatorium = null;

            using (Stream reader = new FileStream(DataFileName, FileMode.Open))
            {
                sanatorium = serializer.Deserialize(reader) as Sanatorium;
            }

            return sanatorium;
        }

        public static void Save(Sanatorium sanatorium)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Sanatorium));

            using (Stream writer = new FileStream(DataFileName, FileMode.Create))
            {
                serializer.Serialize(writer, sanatorium);

                writer.Close();
            }
        }

        private Sanatorium()
        {
            Name = "Санаторий профилакторий \"Молодежный\"";
            About = "Много положительных откликов";
            Phones = new List<string> {"8 904-345-3456, 77-34-56"};
            RestServices = new List<string> { "Бассейн", "Вечерний бар"};
            Description = "В санатории имеется полный перечень услуг  пятизвездочного класса. Возможно посещение отдыхающих родственниками и знакомыми с 10.00 до 22.00";

        }




    }
}
