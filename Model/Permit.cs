﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSanatorium.Api
{
    /// <summary>
    /// Путевка
    /// </summary>
    [Table("Путевка")]
    public class Permit : Entity, IEntity
    {
        [Column("Код_путевки")]
        public int Id { get; set; }

        [Column("Название")]
        public String Name { get; set; }
    }

    public class PermitMap : EntityTypeConfiguration<Permit>
    {
        public PermitMap()
        {

        }
    }
}
