﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model
{
    /// <summary>
    /// Оказываемые услуги
    /// </summary>
    [Table("Специальность")]
    public class Service
    {
        [Column("Код_специальности")]
        public int Id { get; set; }
        
        /// <summary>
        /// Наименование услуги
        /// </summary>
        [Column("Описание")]
        public string Name { get; set; }
        
    }
}
