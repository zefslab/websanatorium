namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoctorPhotoChangeType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Врач", "Фотография", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Врач", "Фотография", c => c.Binary());
        }
    }
}
