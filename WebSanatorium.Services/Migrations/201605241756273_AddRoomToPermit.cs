namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoomToPermit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Кли_Пут", "Код_номера", c => c.Int(nullable: false));
            CreateIndex("dbo.Кли_Пут", "Код_номера");
            AddForeignKey("dbo.Кли_Пут", "Код_номера", "dbo.Номер", "Код_номера", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Кли_Пут", "Код_номера", "dbo.Номер");
            DropIndex("dbo.Кли_Пут", new[] { "Код_номера" });
            DropColumn("dbo.Кли_Пут", "Код_номера");
        }
    }
}
