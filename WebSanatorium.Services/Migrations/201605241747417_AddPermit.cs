namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPermit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Кли_Пут",
                c => new
                    {
                        Код_кли_пут = c.Int(nullable: false, identity: true),
                        Дата_заезда = c.DateTime(nullable: false),
                        Дата_выезда = c.DateTime(nullable: false),
                        Код_клиента = c.Int(nullable: false),
                        Код_путевки = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Код_кли_пут)
                .ForeignKey("dbo.Клиент", t => t.Код_клиента, cascadeDelete: true)
                .ForeignKey("dbo.Путевка", t => t.Код_путевки)
                .Index(t => t.Код_клиента)
                .Index(t => t.Код_путевки);
            
            CreateTable(
                "dbo.Путевка",
                c => new
                    {
                        Код_путевки = c.Int(nullable: false, identity: true),
                        Название = c.String(),
                    })
                .PrimaryKey(t => t.Код_путевки);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Кли_Пут", "Код_путевки", "dbo.Путевка");
            DropForeignKey("dbo.Кли_Пут", "Код_клиента", "dbo.Клиент");
            DropIndex("dbo.Кли_Пут", new[] { "Код_путевки" });
            DropIndex("dbo.Кли_Пут", new[] { "Код_клиента" });
            DropTable("dbo.Путевка");
            DropTable("dbo.Кли_Пут");
        }
    }
}
