namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixEntitiesTebles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Заявка",
                c => new
                    {
                        Код_заявки = c.Int(nullable: false, identity: true),
                        ФИО = c.String(),
                        Телефон = c.String(),
                        эл_почта = c.String(),
                        ДатаЗаезда = c.DateTime(nullable: false),
                        ВключаяЛечение = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Код_заявки);
            
            CreateTable(
                "dbo.Пользователь",
                c => new
                    {
                        Код_пользователя = c.Int(nullable: false, identity: true),
                        ФИО = c.String(),
                        Логин = c.String(),
                        Пароль = c.String(),
                    })
                .PrimaryKey(t => t.Код_пользователя)
                .Index(t => t.Код_пользователя);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Пользователь", new[] { "Код_пользователя" });
            DropTable("dbo.Пользователь");
            DropTable("dbo.Заявка");
        }
    }
}
