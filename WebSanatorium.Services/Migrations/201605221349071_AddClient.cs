namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClient : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Клиент",
                c => new
                    {
                        Код_клиента = c.Int(nullable: false, identity: true),
                        ФИО_клиента = c.String(),
                        Телефон = c.String(),
                        order_id = c.Int(),
                    })
                .PrimaryKey(t => t.Код_клиента)
                .ForeignKey("dbo.Заявка", t => t.order_id)
                .Index(t => t.order_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Клиент", "order_id", "dbo.Заявка");
            DropIndex("dbo.Клиент", new[] { "order_id" });
            DropTable("dbo.Клиент");
        }
    }
}
