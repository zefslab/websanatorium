// <auto-generated />
namespace Dao.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddPermit : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddPermit));
        
        string IMigrationMetadata.Id
        {
            get { return "201605241747417_AddPermit"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
