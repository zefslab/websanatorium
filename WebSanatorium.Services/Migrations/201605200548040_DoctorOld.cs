namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoctorOld : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Врач", "Возраст", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Врач", "Возраст");
        }
    }
}
