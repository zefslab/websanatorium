namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApproved2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Заявка", "ЗаявкаПодтверждена", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Заявка", "ЗаявкаПодтверждена");
        }
    }
}
