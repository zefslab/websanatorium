namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {

            
            CreateTable(
                "dbo.Пользователь",
                c => new
                    {
                        Код_пользователя = c.Int(nullable: false, identity: true),
                        ФИО = c.String(),
                        Логин = c.String(),
                        Пароль = c.String(),
                    })
                .PrimaryKey(t => t.Код_пользователя)
                .Index(t => t.Код_пользователя);
            
        }
        
        public override void Down()
        {

            DropIndex("dbo.Пользователь", new[] { "Код_пользователя" });

            DropTable("dbo.Пользователь");
           
        }
    }
}
