namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Врач", "Фотография", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Врач", "Фотография");
        }
    }
}
