﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSanatorium.Api;

namespace Dao
{
    public class DbConnection : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ///регистрируются в контексте сущностные классы
            modelBuilder.Configurations.Add(new DoctorMap());
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new SpecialtyMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RoomMap());
            modelBuilder.Configurations.Add(new ClientMap());
            modelBuilder.Configurations.Add(new ClientPermitMap());
            modelBuilder.Configurations.Add(new PermitMap());
            
        }

        public virtual DbSet<TEntity> GetSet<TEntity>()
            where TEntity : Entity
        {
            return this.Set<TEntity>();
        }

        public DbSet<WebSanatorium.Api.Order> Orders { get; set; }

        public DbSet<WebSanatorium.Api.Client> Clients { get; set; }

        public DbSet<WebSanatorium.Api.Room> Rooms { get; set; }

        public DbSet<WebSanatorium.Api.RoomClass> RoomClasses { get; set; }

        public DbSet<WebSanatorium.Api.Doctor> Doctors { get; set; }

        public DbSet<WebSanatorium.Api.Specialty> Specialties { get; set; }

        public DbSet<WebSanatorium.Api.ClientPermit> ClientPermits { get; set; }
    }
}
