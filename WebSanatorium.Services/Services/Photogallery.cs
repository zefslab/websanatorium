﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace WebSanatorium.Services
{
    /// <summary>
    /// Функционал управления фотогалереей
    /// </summary>
    public static class Photogallery
    {
        private static string folderName = ConfigurationManager.AppSettings["PhotogalleryFolder"];

        /// <summary>
        /// Просматривается папка с фотографиями, и если в ней находятся файлы,
        /// то возвращается список названий этих файлов 
        /// </summary>
        /// <returns></returns>
        public static string [] GetAll(string folder)
        {
            //Проверяется наличие папки с фотографиями
            if (!Directory.Exists(folder))
            {
                return new string [] {};
            }
            var imgTypes = new string[] { ".jpg", ".png", ".gif", ".bmp", ".pcx" };
            //читаем имена файлов из папки и передаем представлению для отображения
            var dir = new DirectoryInfo(folder);
            return dir.GetFiles().Where(f => imgTypes.Contains(f.Extension.ToLower())).Select(f => f.Name).ToArray();
        }

        /// <summary>
        /// Добавление фотографии
        /// </summary>
        /// <returns></returns>
        public static bool Add(byte[] file)
        {
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }
            ///Добавление файла в директорию
            return true;
        }

        /// <summary>
        /// Удаление фотографии
        /// </summary>
        /// <param name="pictureName"></param>
        /// <returns></returns>
        public static bool Delete(string pictureName)
        {
            if (File.Exists(pictureName))
            {
                File.Delete(pictureName);
                return true;
            }

            return false;
        }
    }
}
