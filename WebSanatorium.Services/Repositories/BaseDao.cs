﻿using System.Collections.Generic;
using System.Linq;
using Dao;
using WebSanatorium.Api;

namespace Service.Repositories
{
    public  class BaseDao<T>
        where T : Entity, IEntity, new ()
    {
        /// <summary>
        /// Контекст доступа к данным в БД
        /// </summary>
        DbConnection db = new DbConnection();

        /// <summary>
        /// Получение записи по ее идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Get(int id)
        {
            return db.GetSet<T>().FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Выбор всех записей
        /// </summary>
        /// <returns></returns>
        public IList<T> GetAll()
        {
            return db.GetSet<T>().ToList();
        }

        /// <summary>
        /// Добавляет запись в БД
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public T Add(T entity)
        {
            var _entity = db.GetSet<T>().Add(entity);

            db.SaveChanges();

            return _entity;

        }
        
        /// <summary>
        /// Удаоение указанной сущности
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            db.GetSet<T>().Remove(entity);

            db.SaveChanges();
        }

        /// <summary>
        /// Удаление  сущности по ее идентификатору
        /// </summary>
        /// <param name="entity"></param>
        public bool Delete(int id)
        {
            var entity = Get(id);

            if (entity != null)
            {
                Delete(entity);
                return true;
            }
            ///Не удалось найти сущность по ее идентификатору чтобы удалить
            return false; 
        }
    }
}
