﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Moq;


namespace WebSanatorium.Db
{
    public static class DbConnectionProvider
    {
        private static DbConnection connection;

        public static DbConnection GetConnection()
        {
            if (connection == null)
            {
                    connection = new DbConnection();
            }
            return connection;
        }

    }
}
