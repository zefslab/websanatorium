﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dao;
using WebSanatorium.Api;
using Service.Repositories;
using WebSanatorium.Db;

namespace WebMvc.Controllers
{
    public class HomeController : Controller
    {
        private DbConnection db = DbConnectionProvider.GetConnection();

        /// <summary>
        /// Главная страница с общими сведениями
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(Sanatorium.GetInstance());
        }
        /// <summary>
        /// Фотогалерея
        /// </summary>
        /// <returns></returns>
        public ActionResult Photogallery()
        {
            return View(WebSanatorium.Services.Photogallery.GetAll(Server.MapPath("~/Content/Files")));
        }
        /// <summary>
        /// Номера
        /// </summary>
        /// <returns></returns>
        public ActionResult Rooms()
        {
            return View(db.Rooms.ToList());
        }
        /// <summary>
        /// Доктора
        /// </summary>
        /// <returns></returns>
        public ActionResult Doctors()
        {
            return View("DoctorsInGrid", db.Specialties.ToList());
        }
        /// <summary>
        /// Медицинские услуги
        /// </summary>
        /// <returns></returns>
        public ActionResult MedicalServices()
        {
            return View(db.Specialties.Select(x=> x.Service).Distinct().ToList());
        }

        public ActionResult Contact()
        {
            return View(Sanatorium.GetInstance());
        }
       
        /// <summary>
        /// Открывается форма на создание новой заявки
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public ActionResult Order(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new Order());
        }

        /// <summary>
        /// Открывается форма на просмотр и редактирование заявки
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        public  ActionResult Order(Order order)
        {
            //Добавляется заявка

            db.Orders.Add(order);

            db.SaveChanges();

            return View("OrderDetails", order);
        }


        [HttpPost]
        public ActionResult OrderDelete(int? id)
        {

            if (id != null)
            {
                //Отменяется заявка
                var order = db.Orders.Find(id);
                
                db.Orders.Remove(order);
            }

            return View("Index");
        }
    }
}