﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dao;
using WebSanatorium.Api;
using WebSanatorium.Db;

namespace WebMvc.Controllers
{
    public class OrdersController : Controller
    {
        private DbConnection db = DbConnectionProvider.GetConnection();

        // GET: Orders
        public ActionResult Index()
        {
            return View(db.Orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FullName,Phone,Email,InputDate,IsWithTherapy, IsApproved")] Order order)
        {
            if (ModelState.IsValid)
            {
                if (order.IsApproved)
                {
                    order.Client = new Client()
                    {
                        FullName = order.FullName,
                        Order = order,
                        Phone = order.Phone
                    };
                }

                db.Orders.Add(order);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            var isApproved = order.IsApproved;
            if (order == null)
            {
                return HttpNotFound();
            }
            
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FullName,Phone,Email,InputDate,IsWithTherapy,IsApproved,Client")] Order order)
        {
            if (ModelState.IsValid)
            {
                var o = db.Orders.Find(order.Id);

                o.IsApproved = order.IsApproved;
                o.AdultCount = order.AdultCount;
                o.ChildrenCount = order.ChildrenCount;
                o.Email = order.Email;
                o.FullName = order.FullName;
                o.InputDate = order.InputDate;
                o.IsWithTherapy = order.IsWithTherapy;
                o.Phone = order.Phone;

                if (order.IsApproved && o.Client == null)
                {
                    o.Client = new Client
                    {
                        FullName = order.FullName,

                        Order = order,
                        Phone = order.Phone
                    };

                    o.Client.Order = o;

                    db.Clients.Add(o.Client);
                }
                else if (order.IsApproved && o.Client != null)
                {
                    o.Client.FullName = order.FullName;
                    o.Client.Phone = order.Phone;
                    o.Client.Order = o;
                }

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            if (order.IsApproved)
            {
                return View("DeleteUnavalable", order);
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

       
    }
}
