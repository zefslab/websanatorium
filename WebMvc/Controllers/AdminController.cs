﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using Dao;
using WebMvc.Models;
using WebSanatorium.Db;

namespace WebMvc.Controllers
{
    public class AdminController : Controller
    {
        private DbConnection db = DbConnectionProvider.GetConnection();

        public ActionResult Photogal()
        {
            return View(WebSanatorium.Services.Photogallery.GetAll(Server.MapPath("~/Content/Files")));
        }
        public ActionResult Reports()
        {
            return View();
        }
        public ActionResult ReportByClientsCreteria()
        {
            var period = new Period(DateTime.Now, DateTime.Now + TimeSpan.FromDays(30));

            return View(period);
        }

        [HttpPost]
        public ActionResult ReportByClientsCreteria([Bind(Include = "BeginDate, EndDate")] Period period)
        {
            ViewBag.Period = period;

            return View("ReportByClients", db.ClientPermits.Where(p => p.BeginDate >= period.BeginDate && p.EndDate <= period.EndDate)
                .ToList());
        }


        public ActionResult ReportBySpecialty()
        {

            return View(db.Doctors.ToList());
        }

        /// <summary>
        /// http://metanit.com/sharp/articles/26.php
        /// Загрузка файлов на сервер
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                // получаем имя файла
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                // сохраняем файл в папку Files в проекте
                upload.SaveAs(Server.MapPath("~/Content/Files/" + fileName));
            }
            return RedirectToAction("Photogal");
        }

        [HttpPost]
        public ActionResult DeleteImg(string img)
        {
            WebSanatorium.Services.Photogallery.Delete(Server.MapPath("~/Content/Files/" + img));

            return RedirectToAction("Photogal");
        }
    }
}