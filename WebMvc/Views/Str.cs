﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.Views
{
    /// <summary>
    /// Используется для единообразия оформления интерфейса
    /// </summary>
    public static class Str
    {
        public static string Edit = "Изменить";
        public static string Details = "Просмотр";
        public static string Delete = "Удалить";
        public static string ToList = "Вернутся к списку";
        public static string Create = "Добавить";
        public static string Save = "Сохранить";


    }
}