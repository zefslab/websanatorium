﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.Models
{
    public class Period
    {
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Period(){}

        public Period(DateTime begin, DateTime end)
        {
            BeginDate = begin;

            EndDate = end;
        }
    }
}